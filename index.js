var fs = require('fs');
var glob = require('glob');
var path = require('path');
var transformer = require('./transformer');
var loaderUtils = require('loader-utils');

module.exports = function(source) {
	this.cacheable();
	var callback = this.async();
	
	var loaderOptions = loaderUtils.parseQuery(this.query);

	var componentFiles = glob.sync(loaderOptions.components, {});
	var transformSource = function(input, components){
		callback(null, transformer(input, components));
	};
	
	var loadedFiles = 0;
	var componentDefinitions = {};
	if(componentFiles){
		componentFiles.forEach(function(filename){
			fs.readFile(filename, 'utf-8', function(err, data){
				if (err) throw err;
				componentDefinitions[path.basename(filename).replace(/\.[^/.]+$/, "")] = data;
				loadedFiles++;
				if(loadedFiles === componentFiles.length){
					transformSource(source, componentDefinitions);
				}
			});
		});
	} else {
		transformSource(source, componentDefinitions);
	}
	
};
