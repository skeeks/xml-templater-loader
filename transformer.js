var Mustache = require('mustache');
var sax = require('sax');
var parser = sax.parser(false, {lowercase: true, position: false});

var elements = [];
var rootNode, error;

// opened a tag.  node has "name" and "attributes" 
parser.onopentag = function (node) {
	var element = {
		'tag': node.name,
		'attrs': node.attributes,
		'children': [],
	};

	if(elements.length !== 0){
		elements[elements.length-1].children.push(element); // set it as children for parent
	} else {
		rootNode = element;
	}
	elements.push(element); // push it to the element chain 
};

// got some text.  t is the string of text. 
// text cannot have children, because of that, we dont need to attach it to the chain.
parser.ontext = function (text) {
	var element = {
		'tag': '$text$',
		'content': text
	};
	elements[elements.length-1].children.push(element);
};

// opened a tag.  node has "name" and "attributes" 
parser.onclosetag = function (node) {
	elements.splice(-1, 1); // remove last candidate of chain.
};

parser.onerror = function(e){
	error = e;
};

function isComponentDefined(name, componentDefs){
	return componentDefs.hasOwnProperty(name);
}

function buildComponent(name, attributes, componentDefs){
	var componentHTML = componentDefs[name];
	var rendered = Mustache.render(componentHTML, attributes);
	return rendered;
}

function transformChildren(children, componentDefs){
	var transformedChildren = [];
	for(var child in children){
		transformedChildren.push(turnToHTML(children[child], componentDefs));
	}
	return transformedChildren;
}

function childrenToString(children, componentDefs){
	return transformChildren(children, componentDefs).join("");
}

function attributesToString(attributes){
	var str = [];
	for(var attr in attributes){
		if(!attributes.hasOwnProperty(attr)){
			continue;
		}
		str.push(attr + '="' + attributes[attr] + '"');
	}
	return str.join(" ");
}

function turnToHTML(xmlTree, componentDefs){
	var name = xmlTree.tag;
	
	if(name === '$text$'){
		return xmlTree.content;
	}
	
	var children = xmlTree.children || [];
	var attributes = xmlTree.attrs || {};
	
	if(isComponentDefined(name, componentDefs)){
		var exAttributes = attributes;
		exAttributes['children'] = transformChildren(children, componentDefs);
		return buildComponent(name, exAttributes, componentDefs);
	} else {
		if(!children || children.length == 0){
			return '<' + name + ' ' + attributesToString(attributes) + ' />'
		} else {
			var html = '<' + name + ' ' + attributesToString(attributes) + ' >';
			html += childrenToString(children, componentDefs);
			html += '</' + name + '>';
			return html;
		}
	}
}

function normalizeDefinitions(definitions){
	var defs = {};
	for(var def in definitions){
		if(!definitions.hasOwnProperty(def)){
			continue;
		}
		defs[def.toLowerCase()] = definitions[def];
	}
	return defs;
}

module.exports = function(xml, definitions) {
	definitions = normalizeDefinitions(definitions);
	parser.write(xml).close();

	if(!rootNode){
		throw new Exception('no root node found');
	}
	//console.log(require('util').inspect(rootNode, {depth: 90}));
	return turnToHTML(rootNode, definitions);
}