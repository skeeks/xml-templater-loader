# xm-templater-loader #

xml-templater-loader is a loader for webpack.

The main goal is to reuse html components:

For example, if you have something like a StringField.

```
#!html

<div class="string-field">
<label for="string-field-1">Name:</label>
<input type="text" id="string-field-1" />
</div>
```

Requirements are, that the structure of this string field (the above html) is always the same.
Only the data changes. If you have, say 100 StringFields on your page, you have 100x times this html.

The disadvantages are clear:

* If you change the html of a StringField (lets say, you want to add the property enabled), you need to modify everywhere, where you use it.
* You html documents are more difficult to maintain, because <StringField /> is easier to read than <div class="string-field">...

The xml-templater solves this problem by providing templates.
You can define components. Components are just some html, with additonal parameters.

File StringField.html
```
#!html
<div class="string-field">
   <label for="string-field-1">Name:</label>
   <input type="text" id="string-field-1" name="{{name}}" />
</div>
```

In the component definition, you can use the Mustache templating language.

Children are always passed as an string array with the variable "children". 

xml-templater will now transform any given document which consists basically out of xml to html.

```
#!xml

<!DOCTYPE html>
<html>
<body>
<StringField name="form1-name" />
</body>
</html>
```

will be transformed into:

```
#!html

<!DOCTYPE html>
<html>
<body>
<div class="string-field">
   <label for="string-field-1">Name:</label>
   <input type="text" id="string-field-1" name="form1-name" />
</div>
</body>
</html>
```
This reduces redundancy in html documents and make it easier to change each component like a StringField

## Limitations ##

* xml-templater-loader cannot handle the string ``<!DOCTYPE html> or any other DOCTYPe at the beginning of the document.
* the file to transform needs to have a root element.