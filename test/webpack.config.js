  
var webpack = require('webpack');
var path = require('path');


var config = {
	entry : './example.xml',
	output : {
		path : '.',
		filename : 'xml-templater-test.min.js'
	},
	module : {
		preLoaders: [
			{ test: /\.xml$/, loader: path.resolve(__dirname, '../'), exclude: '/node_modules/', query: 'components=**/*.html' },
		],
		loaders : [ 
			{ test: /\.xml$/, loader: 'xml-loader' } // will load all .xml files with xml-loader by default 
		],
	},
	debug: true,
	bail: true
};

module.exports = config;
