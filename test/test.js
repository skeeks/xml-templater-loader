var transformer = require('./transformer');

var definitions = {
	'StringField': '<div class="string-field"><input type="text" name="{{name}}{{^name}}default-name{{/name}}" value="{{value}}" /></div>',
	'GroupBox': '<div class="group-box">{{#children}}<div class="group-box-child">{{{children}}}</div>{{/children}}</div>'
};

var source = '' +
'<html>' +
'<StringField value="xx" /> ' +
'<GroupBox class="2">' +
'<StringField name="nachname" /> ' +
'<StringField name="vorname" /> ' +
'</GroupBox>' +
'</html>';

console.log(transformer(source, definitions));